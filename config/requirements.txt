Django==1.5.2
MySQL-python
django-suit
South
django-image-cropping
easy_thumbnails
django-ckeditor
django-tagging
git+git://github.com/gkuhn1/django-admin-multiupload.git
django-oembed
django-embed
lxml
pygeocoder