from apps.materias.models import Secretaria
from apps.multimidia.models import Banner
from apps.servicos.models import Servicos

def estaticos(request):

    banner_topo = Banner.objects.filter(posicao=3, status=True).order_by('-publicado_em')
    banner_sidebar = Banner.objects.filter(posicao=2, status=True).order_by('-publicado_em')[:1]
    servico_menu = Servicos.objects.filter(menu=True)
    servico_sidebar = Servicos.objects.filter(sidebar=True)
    secretarias = Secretaria.objects.filter(status=True)

    return { 'banner_topo':banner_topo,
             'banner_sidebar':banner_sidebar,
             'servico_menu':servico_menu,
             'servico_sidebar':servico_sidebar,
             'secretarias':secretarias, }