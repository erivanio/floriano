# Django settings for django_skeleton project.

import os

# Set here the name of your project
PROJECT_NAME = 'floriano'
PROJECT_ROOT = os.path.join(os.path.dirname(__file__), '..')
location = lambda x: os.path.join(PROJECT_ROOT, x)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'floriano',
        'USER': 'root',
        'PASSWORD': 'mysql321',
        'HOST': '',
        'PORT': '',
    }
}

ALLOWED_HOSTS = []

TIME_ZONE = 'America/Fortaleza'

LANGUAGE_CODE = 'pt-br'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = location('media/')

MEDIA_URL = '/media/'

STATIC_ROOT = location('staticroot/')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    location('static/'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Set your key here
# Make this unique, and don't share it with anybody.
SECRET_KEY = 'kn4390sjdkln23k9ansnr4u4asbj1gj'
from django.conf import global_settings

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'floriano.context_processor.estaticos',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = PROJECT_NAME + '.urls'

WSGI_APPLICATION = PROJECT_NAME + '.wsgi.application'

TEMPLATE_DIRS = (
    location('templates/'),
)

from easy_thumbnails.conf import Settings as thumbnail_settings
THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS

INSTALLED_APPS = (
    'ckeditor',
    'easy_thumbnails',
    'image_cropping',
    'tagging',
    'multiupload',
    'suit',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.admin',
    'south',
    'apps.materias',
    'apps.destaques',
    'apps.multimidia',
    'apps.servicos',
    'apps.cidade',
)



LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SUIT_CONFIG = {
    'ADMIN_NAME': 'Prefeitura de Floriano'
}

#CKEditor
CKEDITOR_UPLOAD_PATH = os.path.join(PROJECT_ROOT, 'media/uploads')

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar':
            # 'Standard',
        [
            {'name': 'basicstyles', 'items': ['Bold', 'Italic', 'Underline']},
            {'name': 'styles', 'items': ['Format', 'FontSize']},
            {'name': 'links', 'items': ['Link', 'Unlink']},
            {'name': 'insert', 'items': ['Image', 'Table', 'SpecialChar'] },
            {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace','-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {'name': 'paragraph', 'items': ['NumberedList', 'BulletedList','-', 'Outdent', 'Indent', '-', 'Blockquote',
                                            '-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                                            '-', 'BidiLtr', 'BidiRtl']},
            {'name': 'tools', 'items': ['Source', 'Maximize']},
        ],
        'height': 300,
        'width': 620,
    },
}

try:
    from local_settings import *
except:
    pass
