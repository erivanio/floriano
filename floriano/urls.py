from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponse
from floriano import views
from django.contrib.sitemaps import Sitemap, FlatPageSitemap
import settings

sitemaps = {
    'site': Sitemap,
    'flatpages': FlatPageSitemap,
    }


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    (r'^$', views.home),
    (r'^contato/$', views.contato),
    (r'^contato/(?P<slug>[\w_-]+)/$', views.contato),


    (r'^noticias/$', 'apps.materias.views.noticias'),
    (r'^noticias/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.materias.views.ver_materia'),
    (r'^noticias/(?P<categoria>[\w_-]+)/', 'apps.materias.views.ver_secretaria'),

    (r'^busca/$', 'apps.materias.views.busca'),

    (r'^albuns/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.multimidia.views.album'),
    (r'^albuns/$', 'apps.multimidia.views.central_albuns'),
    (r'^videos/$', 'apps.multimidia.views.videos'),

    (r'^a-cidade/$', 'apps.cidade.views.cidade'),
    (r'^equipe/$', 'apps.servicos.views.equipe'),
    (r'^servicos/(?P<slug>[\w_-]+)/$', 'apps.servicos.views.servico'),
    (r'^servicos/$', 'apps.servicos.views.servicos'),
    (r'^obras/$', 'apps.servicos.views.obra'),
    (r'^licitacoes/$', 'apps.servicos.views.licitacao'),
    (r'^legislacoes/$', 'apps.legislacao.views.legislacoes'),

    (r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nAllow: /", mimetype="text/plain")),
    (r'^sitemap\.xml$', 'floriano.views.sitemap'),

)

#urlpatterns += patterns('django.contrib.flatpages.views',
#    url(r'^sobre-floriano/$','flatpage', {'url': '/sobre-floriano/'}, name='sobre'),
#    url(r'^principais-eventos/$','flatpage', {'url': '/principais-eventos/'}, name='eventos'),
#    url(r'^generalidades/$','flatpage', {'url': '/generalidades/'}, name='generalidades'),
#    url(r'^educacao-e-economia/$','flatpage', {'url': '/educacao-e-economia/'}, name='economia'),
#)

urlpatterns += patterns('',
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    (r'^ckeditor/', include('ckeditor.urls')),
    (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
