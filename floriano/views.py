from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.destaques.models import Destaque
from apps.materias.models import Materias
from apps.multimidia.models import Video, Album, Banner
from django.core.mail import send_mail
from django.http import HttpResponseRedirect

ASSUNTOS = ('ouvidoria', 'sala-de-imprensa', 'reclamacoes')

def home(request):
    slides = Destaque.objects.filter(posicao=1).order_by('-publicado_em')[:6]
    destaque1 = Destaque.objects.filter(posicao=2).order_by('-publicado_em')[:1]
    destaque2 = Destaque.objects.filter(posicao=3).order_by('-publicado_em')[:1]
    destaque3 = Destaque.objects.filter(posicao=4).order_by('-publicado_em')[:1]
    ultimas = Materias.objects.filter(status=True).order_by('-publicado_em')[:4]
    videos = Video.objects.filter(status=True).order_by('-publicado_em')[:1]
    albuns = Album.objects.filter(status=True).order_by('-publicado_em')[:3]
    banner_medio = Banner.objects.filter(posicao=1, status=True).order_by('-publicado_em')[:1]

    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def contato(request, slug=''):
    if request.POST:
        form = request.POST
        assunto = form.get('assunto').lower().replace(' ', '-')

        origem = form.get('email')
        mensagem = form.get('comentario') + "\n" + origem + "\n" + form.get('nome')
        if assunto == ASSUNTOS[0]:
            destino = ['ouvidoria@floriano.pi.gov.br']
        elif assunto == ASSUNTOS[1]:
            destino = ['comunicacao@floriano.pi.gov.br']
        elif assunto == ASSUNTOS[2]:
            destino = ['ouvidoria@floriano.pi.gov.br']
        send_mail(assunto, mensagem, origem, recipient_list=destino)
        return HttpResponseRedirect('/')
    else:
        assuntos = ASSUNTOS
        selected = slug
    return render_to_response('floriano/contato.html', locals(), context_instance=RequestContext(request))

def sitemap(request):
    p = request.GET.get('p',1)
    vmin = ((1000 * int(p)) - 1000)
    vmax = 1000 * int(p)

    stmapgr = Materias.objects.filter(status=True).order_by('-id')[vmin:vmax]

    return render_to_response('xml/geral.xml', locals(),
                              context_instance=RequestContext(request))
