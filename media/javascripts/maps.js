
var map = L.map('map').setView([-6.761358,-43.01028],13);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);
for (i = 0; i < obras.length; i++){
    var _obra = obras[i];
    L.marker([_obra.lat, _obra.lng]).addTo(map).bindPopup('<b>Obra: </b>'+_obra.nome+'</br><b>Endereço: </b>'+ _obra.endereco+'<br><b>Descrição:</b> <br>'+_obra.descricao);
}