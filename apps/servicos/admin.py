# -*- encoding: utf-8 -*-
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from apps.servicos.models import Servicos, Equipe, Obra, Licitacao

class EquipeAdmin(ImageCroppingMixin, admin.ModelAdmin):

    list_display = ('nome', 'cargo', 'foto_thumbnail')
    list_filter = ['cargo', 'secretaria']
    search_fields = ['nome', 'email', 'cargo', 'secretaria']
    date_hierarchy = 'data_cadastro'


    def foto_thumbnail(self, obj):
        try:
            return u'<img width="100" src="/media/%s" />' % (obj.foto)
        except:
            return "Sem Imagem"
    foto_thumbnail.short_description = 'Foto'
    foto_thumbnail.allow_tags = True

class ObrasAdmin(ImageCroppingMixin, admin.ModelAdmin):

    fieldsets = [
                 (None, {'fields':[('nome'), 'slug', 'descricao']}),
                 ('Endereço', {'fields':[('endereco'), 'lat', 'lng']}),
                 ('Depoimento', {'fields':[('depoimento'),'foto_depoimento','foto_depoimento_thumb']}),
    ]
    list_display = ('nome', 'created_at', 'endereco')
    list_filter = ['created_at', 'modified_at']
    search_fields = ['nome', 'endereco']
    date_hierarchy = 'created_at'

    def save_model(self, request, obj, form, change):
        if not obj.lat and not obj.lng:
            import unicodedata
            import re
            from pygeocoder import Geocoder
            try:
                value = unicodedata.normalize('NFKD', obj.endereco).encode('ascii', 'ignore')
                value = unicode(re.sub('[^\w\s-]', '', value).strip())

                address = "%s, Floriano, Piaui, Brazil" % (value)
                #print address
                results = Geocoder.geocode(address)
                #print results[0]
                obj.lng = results[0].coordinates[1]
                obj.lat = results[0].coordinates[0]
            except:
                pass
        obj.save()

admin.site.register(Obra, ObrasAdmin)
admin.site.register(Servicos)
admin.site.register(Licitacao)
admin.site.register(Equipe, EquipeAdmin)