# -*- encoding: utf-8 -*-
from datetime import datetime

from django.db import models
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from django.db.models import signals


TIPO_CHOICES = (('0', 'Servicos'), ('1', 'Links'))
CARGO_CHOICES = (('0', 'Prefeitura'), ('1', 'Vice-prefeitura'), ('2', 'Secretaria'))
MODALIDADE_CHOICES = (
    ('Licitacao', 'Licitação'),
    ('Tomada de Preco','Tomada de Preço'),
    ('Carta Convite', 'Carta Convite'),
    ('Pregao', 'Pregão'),
    ('Edital de Credenciamento', 'Edital de Credenciamento')
)

class Licitacao(models.Model):

    titulo = models.CharField(max_length = 200)
    modalidade = models.CharField(max_length = 30, choices = MODALIDADE_CHOICES)
    objeto = models.TextField()
    data_publicacao = models.DateField(default = datetime.now)
    data_abertura = models.DateTimeField(default= datetime.now)
    edital = models.FileField(upload_to = 'editais/%Y/%m/')

    def __unicode__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Licitação'
        verbose_name_plural = 'Licitações'

class Servicos(models.Model):
    nome = models.CharField(max_length=100)
    link = models.CharField(max_length=100, blank=True, null=True)
    classe_icone = models.CharField(max_length=30)
    icon = models.ImageField("ícone", upload_to='servicos/', blank=True, help_text="Ícone dos Serviços. Dimensões 91x91")
    descricao = RichTextField(blank=True, null=True)
    tipo = models.CharField(max_length=1, choices=TIPO_CHOICES)
    sidebar = models.BooleanField(default=True)
    menu = models.BooleanField(default=True)

    def __unicode__(self):
        return self.nome

    def get_absolute_url(self):
        if self.tipo == '0':
            return '/servicos/%s/' % (self.link)
        else:
            return self.link

    class Meta:
        verbose_name_plural = 'Serviços'


class Equipe(models.Model):
    nome = models.CharField(max_length=100)
    cargo = models.CharField(max_length=1, choices=CARGO_CHOICES)
    secretaria = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    data_cadastro = models.DateField(auto_now_add=True)
    foto = models.ImageField(upload_to='equipe/', blank=True, help_text="Foto perfil do funcionário. Dimensões 75x75")
    foto_thumb = ImageRatioField('foto', '75x75')

    def get_thumb(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (75, 75),
                    'box': self.foto_thumb,
                    'crop': True,
                    'detail': True,
                }).url

    def __unicode__(self):
        return self.nome


class Obra(models.Model):
    nome = models.CharField(max_length = 200)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    descricao = models.TextField()
    endereco = models.CharField(max_length = 200, null = True)
    lat = models.CharField(max_length=30, blank=True, null=True)
    lng = models.CharField(max_length=30, blank=True, null=True)
    depoimento = models.CharField("Depoimento de um popular",max_length = 200, blank = True, null = True)
    foto_depoimento = ThumbnailerImageField(upload_to='depoimentos/%Y/%m/',blank = True, null = True)
    foto_depoimento_thumb = ImageRatioField('foto_depoimento', '640x300')
    created_at = models.DateTimeField(auto_now_add = True)
    modified_at = models.DateTimeField(auto_now_add = True)

    def get_thumb_depoimento(self):
        return get_thumbnailer(self.foto_depoimento).get_thumbnail({
                    'size': (640,300),
                    'box': self.foto_depoimento_thumb,
                    'crop': True,
                    'detail': True,
                }).url

    def __unicode__(self):
        return self.nome

def obra_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nome)
signals.pre_save.connect(obra_pre_save, sender=Obra)