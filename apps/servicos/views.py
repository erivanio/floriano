# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from apps.servicos.models import Equipe, Servicos, Obra, Licitacao
from django.core.paginator import Paginator, EmptyPage, InvalidPage


def servico(request, slug):
   servico = get_object_or_404(Servicos, link=slug)
   return render_to_response('floriano/servico.html', locals(),
                         context_instance=RequestContext(request))

def servicos(request):
   servicos = Servicos.objects.all()
   return render_to_response('floriano/servicos.html', locals(),
                         context_instance=RequestContext(request))

def equipe(request):
    prefeito = Equipe.objects.filter(cargo=0).order_by('-data_cadastro')[:1]
    vice = Equipe.objects.filter(cargo=1).order_by('-data_cadastro')[:1]
    secretarios = Equipe.objects.filter(cargo=2)

    return render_to_response('floriano/equipe.html', locals(),
                          context_instance=RequestContext(request))

def obra(request):
    obras = Obra.objects.all()
    return render_to_response('floriano/obras.html', locals(),
                          context_instance=RequestContext(request))

def licitacao(request):
    licitacoes = Licitacao.objects.all()
    paginator = Paginator(licitacoes, 4)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        licitacoes = paginator.page(page)
    except (EmptyPage, InvalidPage):
        licitacoes = paginator.page(paginator.num_pages)

    return render_to_response('floriano/licitacoes.html', locals(),
                          context_instance=RequestContext(request))