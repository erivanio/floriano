# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Servicos.classe_icone_grande'
        db.delete_column(u'servicos_servicos', 'classe_icone_grande')

        # Adding field 'Servicos.icon'
        db.add_column(u'servicos_servicos', 'icon',
                      self.gf('django.db.models.fields.files.ImageField')(default=None, max_length=100, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Servicos.classe_icone_grande'
        raise RuntimeError("Cannot reverse this migration. 'Servicos.classe_icone_grande' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Servicos.classe_icone_grande'
        db.add_column(u'servicos_servicos', 'classe_icone_grande',
                      self.gf('django.db.models.fields.CharField')(max_length=30),
                      keep_default=False)

        # Deleting field 'Servicos.icon'
        db.delete_column(u'servicos_servicos', 'icon')


    models = {
        u'servicos.equipe': {
            'Meta': {'object_name': 'Equipe'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'data_cadastro': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'secretaria': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'servicos.licitacao': {
            'Meta': {'object_name': 'Licitacao'},
            'data_abertura': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'data_publicacao': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'edital': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidade': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'objeto': ('django.db.models.fields.TextField', [], {}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'servicos.obra': {
            'Meta': {'object_name': 'Obra'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'depoimento': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'descricao': ('django.db.models.fields.TextField', [], {}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'foto_depoimento': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'foto_depoimento_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'lng': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'})
        },
        u'servicos.servicos': {
            'Meta': {'object_name': 'Servicos'},
            'classe_icone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'descricao': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sidebar': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['servicos']