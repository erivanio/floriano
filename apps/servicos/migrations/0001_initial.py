# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Servicos'
        db.create_table(u'servicos_servicos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('classe_icone', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('descricao', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('sidebar', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('menu', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'servicos', ['Servicos'])

        # Adding model 'Equipe'
        db.create_table(u'servicos_equipe', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('secretaria', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('data_cadastro', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'servicos', ['Equipe'])


    def backwards(self, orm):
        # Deleting model 'Servicos'
        db.delete_table(u'servicos_servicos')

        # Deleting model 'Equipe'
        db.delete_table(u'servicos_equipe')


    models = {
        u'servicos.equipe': {
            'Meta': {'object_name': 'Equipe'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'data_cadastro': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'secretaria': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'servicos.servicos': {
            'Meta': {'object_name': 'Servicos'},
            'classe_icone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'descricao': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sidebar': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['servicos']