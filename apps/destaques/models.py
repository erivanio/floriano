# -*- encoding: utf-8 -*-
from django.db import models
from apps.materias.models import Materias
from datetime import datetime
import sys, time
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from django.db.models import signals


DESTAQUES_POSICAO_CHOICES = (
   ('1', 'Principal 922x414'),
   ('2', 'Destaque Texto 1'),
   ('3', 'Destaque Texto 2'),
   ('4', 'Destaque Texto 3'),
)


class Destaque(models.Model):

    filepath = 'uploads/destaques/%Y/%m/'

    posicao = models.CharField(verbose_name='Posição de Destaque', max_length=2, choices=DESTAQUES_POSICAO_CHOICES)
    materia = models.ForeignKey(Materias, blank=True, null=True)
    link = models.CharField(max_length=100, help_text="Link para a matéria", blank=True, null=True)
    status = models.BooleanField(default = True)
    titulo = models.CharField(max_length=100, help_text="Para Manchetes, Destaques com Foto e Destaques sem Foto")
    subtitulo = models.CharField(max_length=100, blank=True, help_text="Para Manchetes")
    chapeu = models.CharField(max_length=255, blank=True, help_text="Para Destaques com Foto e Destaques sem Foto")
    foto = models.ImageField(blank=True, null=True, upload_to=filepath)
    foto_medium = ImageRatioField('foto', '680x325')
    foto_thumb = ImageRatioField('foto', '400x150')
    foto_small = ImageRatioField('foto', '210x115')
    publicado_em = models.DateTimeField(default = datetime.now)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        if self.materia:
            return self.materia.get_absolute_url()
        return self.link

    def get_destaque_principal(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (680, 325),
                    'box': self.foto_medium,
                    'crop': True,
                    'detail': True,
                }).url

    def get_destaque_secundario(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (400, 150),
                    'box': self.foto_thumb,
                    'crop': True,
                    'detail': True,
                }).url

    def get_destaque_small(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (210, 115),
                    'box': self.foto_small,
                    'crop': True,
                    'detail': True,
                }).url