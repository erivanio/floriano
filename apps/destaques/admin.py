from django.contrib import admin
from image_cropping import ImageCroppingMixin
from apps.destaques.models import Destaque
from apps.destaques.forms import DestaqueForm


class DestaqueAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('materia', 'posicao')
    form = DestaqueForm

admin.site.register(Destaque, DestaqueAdmin)
