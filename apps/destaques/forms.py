from django import forms
from apps.destaques.models import Destaque
from apps.materias.models import Materias


class DestaqueForm(forms.ModelForm):
    titulo = forms.CharField(max_length=60)
    # titulo.max_length = 50

    class Meta:
        model = Destaque
        exclude = ('created_at', 'subtitulo')

    def clean_foto(self):
        if self.cleaned_data['posicao'] == '1' and not self.cleaned_data['foto']:
            raise forms.ValidationError("Esse tipo de destaque exige foto")
        return self.cleaned_data['foto']

    def __init__(self, *args, **kwargs):
        super(DestaqueForm, self).__init__(*args, **kwargs)
        try:
            materia_id = kwargs['initial']['materia_id']
            m = Materias.objects.get(id=materia_id)
            self.initial['materia'] = materia_id
            self.initial['titulo'] = m.titulo
        except Exception, e:
            pass