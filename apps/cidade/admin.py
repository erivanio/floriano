from django.contrib import admin
from apps.cidade.models import Cidade

class CidadeAdmin(admin.ModelAdmin):
    list_display = ('titulo',)

admin.site.register(Cidade, CidadeAdmin)
