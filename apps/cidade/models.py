# coding=utf-8
from django.db import models
from ckeditor.fields import RichTextField

class Cidade(models.Model):

    titulo = models.CharField(max_length=200)
    conteudo = RichTextField(verbose_name='Conteúdo')

    class Meta:
        verbose_name_plural = 'A Cidade'
        verbose_name = 'A Cidade'

    def __unicode__(self):
        return self.titulo