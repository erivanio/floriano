# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cidade'
        db.create_table(u'cidade_cidade', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('conteudo', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal(u'cidade', ['Cidade'])


    def backwards(self, orm):
        # Deleting model 'Cidade'
        db.delete_table(u'cidade_cidade')


    models = {
        u'cidade.cidade': {
            'Meta': {'object_name': 'Cidade'},
            'conteudo': ('ckeditor.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['cidade']