# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.cidade.models import Cidade


def cidade(request, ):
   sobre = Cidade.objects.all()[:4]
   return render_to_response('floriano/sobre.html', locals(), context_instance=RequestContext(request))