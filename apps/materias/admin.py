# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib import admin
from image_cropping import ImageCroppingMixin

#models
from apps.materias.models import Secretaria, Materias, Arquivos
from apps.materias.forms import MateriasFormAdmin
from django.contrib.flatpages.models import FlatPage

# Note: we are renaming the original Admin and Form as we import them!
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from django import forms
from ckeditor.widgets import CKEditorWidget

class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = FlatPage # this is not automatically inherited from FlatpageFormOld

class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)

class ArquivosAdmin(admin.ModelAdmin):
    list_display = ('nome', 'link')
    fields = ['nome', 'arquivo']

admin.site.register(Arquivos,ArquivosAdmin)

class MateriasAdmin(ImageCroppingMixin, admin.ModelAdmin):

    list_per_page = 50
    form = MateriasFormAdmin
    # inlines = [FraseInline]
    fieldsets = [
                 (None, {'fields':[('titulo', 'subtitulo'), 'publicado_em', 'texto', 'fonte', 'autor', 'palavras_chaves']}),
                 ('Secretaria', {'fields':[('secretaria')]}),
                 ('Midia', {'fields':['foto', ('legenda', 'credito'),'foto_materia','foto_destaque','foto_destaque_materia', 'album']}),
                 ('Mais Informacoes', {'fields':['status']}),
    ]
    list_display = ('titulo', 'publicado_em', 'foto_thumbnail', 'criador', 'visualizacoes','get_actions')
    list_filter = ['publicado_em', 'secretaria', 'status', 'criador']
    search_fields = ['titulo', 'subtitulo']
    date_hierarchy = 'publicado_em'

    def queryset(self, request):
        queryset = super(MateriasAdmin, self).queryset(request)
        try:
            q = queryset.filter(secretaria=request.user.get_profile().secretaria)
        except:
            q = queryset.filter()
        return q

    def save_model(self, request, obj, form, change):
        if not change:
            obj.cadastrado_em = datetime.today()
            obj.criador = request.user
        obj.alterador = request.user
        obj.alterado_em = datetime.today()
        try:
            obj.secretaria = request.user.get_profile().secretaria
        except:
            pass
        obj.save()

    def foto_thumbnail(self, obj):
        try:
            return u'<img width="100" src="/media/%s" />' % (obj.foto)
        except:
            return "Sem Imagem"
    foto_thumbnail.short_description = 'Foto'
    foto_thumbnail.allow_tags = True

    def get_actions(self, obj):
        try:
            if obj.id:
                return u'<a class="add-another" onclick="return showAddAnotherPopup(this);" href="/admin/destaques/destaque/add/?_popup=1&materia_id=%s">Add Destaque</a>\
                        <a target="blank" href="%s">Ver no site</a>' % (obj.id, obj.get_absolute_url())
        except:
            return
    get_actions.allow_tags = True
    get_actions.short_description = "Ações"

admin.site.register(Materias, MateriasAdmin)

class secretariaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"url": ("nome",)}

    list_display = ('nome', 'url', 'descricao', 'dt_cadastro', 'dt_ultAtualizacao', 'status')
    list_filter = ['dt_cadastro', 'dt_ultAtualizacao', 'status']
    search_fields = ['nome']
    date_hierarchy = 'dt_cadastro'

admin.site.register(Secretaria, secretariaAdmin)
