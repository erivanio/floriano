# -*- coding: utf-8 -*-
from datetime import datetime
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from apps.materias.models import Materias, Secretaria
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q


def noticias(request):
    noticias = Materias.objects.filter(status=True).exclude(publicado_em__gte=datetime.today()).order_by('-publicado_em')
    secretarias = Secretaria.objects.filter(status=True)
    paginator = Paginator(noticias, 10)
    titulo = 'Notícias'
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('floriano/listagem.html', locals(),
                              context_instance=RequestContext(request))

def ver_materia(request, slug, id):
    noticia = get_object_or_404(Materias, slug=slug, id=id, status=True)
    secretarias = Secretaria.objects.filter(status=True)
    relacionadas = Materias.objects.filter(secretaria=noticia.secretaria, status=True).exclude(publicado_em__gte=datetime.today()).exclude(id=noticia.id)[:2]
    tags = noticia.palavras_chaves.split(',')
    try:
        titulo = noticia.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('floriano/ver.html', locals(),
                              context_instance=RequestContext(request))

def ver_secretaria(request, categoria):
    secretaria = get_object_or_404(Secretaria, url=categoria)
    noticias = Materias.objects.filter(secretaria=secretaria, status=True).exclude(publicado_em__gte=datetime.today()).order_by('-publicado_em')
    secretarias = Secretaria.objects.filter(status=True)
    paginator = Paginator(noticias, 10)
    titulo = 'Notícias'
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('floriano/listagem.html', locals(),
                              context_instance=RequestContext(request))
def busca(request):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (
                Q(titulo__icontains=queryb) |
                Q(subtitulo__icontains=queryb) |
                Q(texto__icontains=queryb)
            )
    except:
        try:
            queryb = request.GET['palavra']
            if queryb:
                qset = (
                    Q(titulo__icontains=queryb) |
                    Q(subtitulo__icontains=queryb) |
                    Q(texto__icontains=queryb)
                )
        except:
            queryb = None
    if queryb:
        noticias = Materias.objects.filter(qset)
    else:
        return render_to_response('404.html', locals(),
                              context_instance=RequestContext(request))
    flagbusca = True
    paginator = Paginator(noticias, 10)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)

    return render_to_response('floriano/listagem.html', locals(),
                              context_instance=RequestContext(request))