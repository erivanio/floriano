# -*- coding: utf-8 -*-
from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^tagging\.fields\.TagField"])
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sitemaps import ping_google
from django.db.models import signals
from django.template.defaultfilters import slugify
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from tagging.fields import TagField
from tagging.models import Tag
from tagging.utils import parse_tag_input

from apps.multimidia.models import Album

class Secretaria(models.Model):
    nome = models.CharField('Nome da secretaria', max_length=35)
    url = models.SlugField('Url da secretaria', max_length=35)
    descricao = models.CharField('Descricao', max_length = 150)
    dt_cadastro = models.DateTimeField(verbose_name='Data de Cadastro', auto_now_add=True)
    dt_ultAtualizacao = models.DateTimeField(verbose_name='Data Ultima Atualizacao', blank=True, default=datetime.now)
    status = models.BooleanField(verbose_name="Status", default=True)
    secretaria_origem = models.ForeignKey('Secretaria', blank=True, null=True)

    class Meta:
        ordering = ('nome',)
        verbose_name = 'secretaria'
        verbose_name_plural = 'Secretarias'

    def get_absolute_url(self):
        return ('/noticias/%s/' % (self.url))

    def __unicode__(self):
        return self.nome

class Arquivos(models.Model):
    class Meta:
        verbose_name = u'Arquivo'
        verbose_name_plural = u'Arquivos'

    nome = models.CharField(max_length=100)
    arquivo = models.FileField(upload_to='arquivos')

    def link(self):
        url = "http://www.floriano.pi.gov.br/media/%s" % (self.arquivo)
        return url

    def __unicode__(self):
        return self.nome

class Materias(models.Model):
    class Meta:
        ordering = ('-publicado_em',)
        verbose_name = u'Materia'
        verbose_name_plural = u'Materias'
        get_latest_by = 'publicado_em'

    titulo = models.CharField(max_length=100, help_text="Titulo da Noticia")
    subtitulo = models.CharField(max_length=100, help_text="Para Manchetes")
    texto = models.TextField(verbose_name="Texto")
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    url = models.SlugField(max_length=200, blank=True, unique=True)

    album = models.ForeignKey(Album, blank=True, null=True,
        help_text="Album relacionado a esta notícia. Caso esta matéria queira exibir mais de uma foto relacionada dentro da notícia.")

    foto = ThumbnailerImageField(upload_to='uploads/materia/%Y/%m/', blank=True, help_text ="Foto destaque na materia. Dimensões 300x250")
    foto_materia = ImageRatioField('foto', '300x250')
    legenda = models.CharField(max_length=50, blank=True,
        help_text="Se uma foto for inserida, a legenda deve ser preenchida! tamanho maximo de 30 caracteres.")
    credito = models.CharField(max_length=30, blank=True,
        help_text="Creditos da foto.")

    foto_destaque = models.ImageField(upload_to='destaques/%Y/%m/', blank=True, help_text="Foto destaque dentro da materia. Dimensões 750x240")
    foto_destaque_materia = ImageRatioField('foto_destaque', '750x240')
    #
    #ativarvideo = models.BooleanField(verbose_name='Utilizar Video', default=False,
    #    help_text="Habilitar o uso de um video relacionado a esta noticia")
    #video = models.ForeignKey(Video, blank=True, null=True, verbose_name='Video',
    #    help_text="Video relacionado a esta noticia. ")

    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)

    ativarcomentarios = models.BooleanField(verbose_name='Utilizar comentarios', default=True,
        help_text="Habilitar o uso de comentarios do Facebook")

    status = models.BooleanField(verbose_name='Noticia Ativa?', default=True,
        help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver esta notícia")
    secretaria = models.ForeignKey(Secretaria, blank=False, null=False)

    visualizacoes = models.IntegerField(default=0)
    criador = models.ForeignKey(User, blank=True, null=True, related_name='criador')
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', blank=True)
    alterador = models.ForeignKey(User, blank=True, null=True, related_name='alterador')
    alterado_em = models.DateTimeField(verbose_name='Data da ultima alteração', blank=True)

    fonte = models.CharField(max_length = 200, null = True, blank = True)
    autor = models.CharField(max_length = 200, null = True, blank = True)
    palavras_chaves = TagField('palavras chaves separadas por vírgula')

    def nomeBusca(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        try:
            return ('/noticias/%s-%i.html' % (self.slug, self.id))
        except:
            pass

    def get_foto(self):
        return "/media/%s" % self.foto

    def save(self, force_insert=False, force_update=False):
        super(Materias, self).save(force_insert, force_update)
        try:
            ping_google()
        except Exception:
            pass

    def get_image_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (300, 250),
                    'box': self.foto_materia,
                    'crop': True,
                    'detail': True,
                }).url

    def get_image_medium_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (310, 150),
                    'box': self.foto_materia,
                    'crop': True,
                    'detail': True,
                }).url

    def get_thumb_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (200, 120),
                    'box': self.foto_materia,
                    'crop': True,
                    'detail': True,
                }).url

    def get_image_full_materia(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
                'size': (750, 240),
                'box': self.foto_destaque_materia,
                'crop': True,
                'detail': True,
            }).url

    def set_tags(self, tags):
        Tag.objects.update_tags(self, tags)

    def get_tags(self, tags):
        return Tag.objects.get_for_object(self)

    def get_palavras_chaves(self):
        return parse_tag_input(self.palavras_chaves)

def materias_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        slug = slugify(instance.titulo)
        instance.slug = slugify(instance.titulo)
        novourl = 'noticias/%s'%(slug)
        instance.url = novourl

def materias_post_save(signal, instance, sender, **kwargs):
    """Este signal atualiza a data de atualização da seção"""
    secretaria = instance.secretaria
    secretaria.dt_ultAtualizacao = datetime.today()
    secretaria.save()


signals.pre_save.connect(materias_pre_save, sender=Materias)
signals.post_save.connect(materias_post_save, sender=Materias)