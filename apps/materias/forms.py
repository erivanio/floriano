# -*- coding: utf-8 -*-
from django import forms
from ckeditor.widgets import CKEditorWidget
from apps.materias.models import Materias


class MateriasFormAdmin(forms.ModelForm):

    class Meta:
        model = Materias
        widgets = {
            'texto': CKEditorWidget()
        }
