# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Secretaria'
        db.create_table(u'materias_secretaria', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=35)),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=35)),
            ('descricao', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('dt_cadastro', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('dt_ultAtualizacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('secretaria_origem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secretaria'], null=True, blank=True)),
        ))
        db.send_create_signal(u'materias', ['Secretaria'])

        # Adding model 'Materias'
        db.create_table(u'materias_materias', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('subtitulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('texto', self.gf('django.db.models.fields.TextField')()),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, blank=True)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, blank=True)),
            ('album', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['multimidia.Album'], null=True, blank=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('foto_materia', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('legenda', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('credito', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('foto_destaque', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('foto_destaque_materia', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('ativarcomentarios', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('secretaria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secretaria'])),
            ('visualizacoes', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('criador', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='criador', null=True, to=orm['auth.User'])),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('alterador', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='alterador', null=True, to=orm['auth.User'])),
            ('alterado_em', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('fonte', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('autor', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('palavras_chaves', self.gf('tagging.fields.TagField')()),
        ))
        db.send_create_signal(u'materias', ['Materias'])


    def backwards(self, orm):
        # Deleting model 'Secretaria'
        db.delete_table(u'materias_secretaria')

        # Deleting model 'Materias'
        db.delete_table(u'materias_materias')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'materias.materias': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Materias'},
            'album': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['multimidia.Album']", 'null': 'True', 'blank': 'True'}),
            'alterado_em': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'alterador': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'alterador'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'ativarcomentarios': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'credito': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'criador': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'criador'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'fonte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_destaque_materia': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_materia': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'palavras_chaves': ('tagging.fields.TagField', [], {}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'secretaria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secretaria']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'subtitulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'materias.secretaria': {
            'Meta': {'ordering': "('nome',)", 'object_name': 'Secretaria'},
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'dt_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dt_ultAtualizacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '35'}),
            'secretaria_origem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secretaria']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '35'})
        },
        u'multimidia.album': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Album'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'creditos': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'foto_big': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_medium': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_small': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['materias']