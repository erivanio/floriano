from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.legislacao.models import Legislacao, LEGISLACAO_CHOICES


def legislacoes(request):
    opcao_legislacao = LEGISLACAO_CHOICES
    legislacoes = Legislacao.objects.all().order_by('-created_at')
    return render_to_response('floriano/legislacao.html', locals(),
                              context_instance=RequestContext(request))