from django.contrib import admin

from apps.legislacao.models import Legislacao


class LegislacaoAdmin(admin.ModelAdmin):
    save_on_top = True

admin.site.register(Legislacao, LegislacaoAdmin)