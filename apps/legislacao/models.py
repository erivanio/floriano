# coding=utf-8
from datetime import datetime
from django.db import models
from django.template.defaultfilters import slugify

def arquivo_upload(instance, filename):
    filename = filename.split('.')
    ext = filename[-1]
    filename = filename[0]
    nome_arquivo = "%s.%s" % (slugify(filename),ext)
    return '/'.join(['legislacoes', nome_arquivo])

LEGISLACAO_CHOICES = (('LOA','LOA'), ('LDO','LDO'), ('PPA', 'PPA'),('Decretos','Decretos'),('Portarias','Portarias'),('Atos','Atos Municipais'))

class Legislacao(models.Model):
    nome = models.CharField(max_length=750)
    descricao = models.CharField(max_length=600, blank=True)
    arquivo = models.FileField(upload_to=arquivo_upload)
    ano = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField("Criado em",null=True, blank=True, default = datetime.today())

    def __unicode__(self):
        return self.nome

    class Meta:
        verbose_name = "Legislação"
        verbose_name_plural = "Legislações"