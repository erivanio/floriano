# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Legislacao.tipo'
        db.delete_column(u'legislacao_legislacao', 'tipo')


    def backwards(self, orm):
        # Adding field 'Legislacao.tipo'
        db.add_column(u'legislacao_legislacao', 'tipo',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=30),
                      keep_default=False)


    models = {
        u'legislacao.legislacao': {
            'Meta': {'object_name': 'Legislacao'},
            'ano': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'arquivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 16, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '750'})
        }
    }

    complete_apps = ['legislacao']