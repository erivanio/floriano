# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Legislacao'
        db.create_table(u'legislacao_legislacao', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=750)),
            ('descricao', self.gf('django.db.models.fields.CharField')(max_length=600, blank=True)),
            ('arquivo', self.gf('django.db.models.fields.files.FileField')(max_length=255, blank=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('ano', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 7, 9, 0, 0), null=True, blank=True)),
        ))
        db.send_create_signal(u'legislacao', ['Legislacao'])


    def backwards(self, orm):
        # Deleting model 'Legislacao'
        db.delete_table(u'legislacao_legislacao')


    models = {
        u'legislacao.legislacao': {
            'Meta': {'object_name': 'Legislacao'},
            'ano': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'arquivo': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 9, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '750'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'})
        }
    }

    complete_apps = ['legislacao']