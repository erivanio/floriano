# -*- coding: utf-8 -*-
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from apps.multimidia.models import *
from image_cropping import ImageCroppingMixin
from multiupload.admin import MultiUploadAdmin

class FotoAdmin(ImageCroppingMixin, MultiUploadAdmin):
    list_display = ('imagemAdmin', 'legenda', 'album')
    search_fields = ['album__titulo']
    list_filter = ['cadastrado_em', 'album']
    # default value of all parameters:
    change_form_template = 'multiupload/change_form.html'
    change_list_template = 'multiupload/change_list.html'
    multiupload_template = 'multiupload/upload.html'
    multiupload_list = True
    multiupload_form = True
    multiupload_maxfilesize = 3 * 2 ** 20 # 3 Mb
    multiupload_minfilesize = 0
    multiupload_acceptedformats = ( "image/jpeg",
                                    "image/jpg",
                                    "image/png",)

    def process_uploaded_file(self, uploaded, object, request):
        '''
        This method will be called for every file uploaded.
        Parameters:
            :uploaded: instance of uploaded file
            :object: instance of object if in form_multiupload else None
            :kwargs: request.POST received with file
        Return:
            It MUST return at least a dict with:
            {
                'url': 'url to download the file',
                'thumbnail_url': 'some url for an image_thumbnail or icon',
                'id': 'id of instance created in this method',
                'name': 'the name of created file',
            }
        '''
        # example:
        album_id = request.POST.get('album_id', [''])
        title = request.POST.get('title', [''])
        f = Foto(foto=uploaded, legenda=title, album_id=album_id)
        f.save()

        return {
            'url': f.imagem(),
            'thumbnail_url': f.imagem(),
            'id': f.id,
            'name': f.legenda
        }

    def delete_file(self, pk, request):
        '''
        Function to delete a file.
        '''
        # This is the default implementation.
        obj = get_object_or_404(self.queryset(request), pk=pk)
        obj.delete()

admin.site.register(Foto, FotoAdmin)

class AlbumAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('imagemAdmin', 'titulo', 'get_actions')
    list_filter = ['titulo']
    search_fields = ['titulo']

    def response_add(self,request,obj,post_url_continue=None):
        return HttpResponseRedirect("/admin/album/foto/multiupload/?album_id=%s" % (obj.id))

    def get_actions(self, obj):
        try:
            return u'<div align="center"><a class="btn btn-success" href="/admin/multimidia/foto/multiupload/?album_id=%s">Adicionar Imagens</a>  <a class="btn btn-info" href="/admin/multimidia/foto/?q=&album__id__exact=%s">Ver Imagens</a></div>' % (obj.id, obj.id)
        except:
            return
    get_actions.short_description = 'Ações'
    get_actions.allow_tags = True

admin.site.register(Album, AlbumAdmin)

class VideoAdmin(admin.ModelAdmin):

    fieldsets = [
                 (None, {'fields':['titulo']}),
                 ('Media', {'fields':['foto', 'video']}),
                 ('Mais Informacoes', {'fields':[('publicado_em', 'status')], 'classes': ['collapse closed']}),
    ]
    list_display = ('titulo', 'publicado_em', 'foto_thumbnail')
    list_filter = ['publicado_em', 'status']
    search_fields = ['titulo']
    date_hierarchy = 'publicado_em'

    def foto_thumbnail(self, obj):
        try:
            return u'<img height="50px" src="%s" />' % (obj.foto)
        except:
            return "Sem Imagem"
    foto_thumbnail.short_description = 'Foto'
    foto_thumbnail.allow_tags = True

    def save_model(self, request, obj, form, change):
        if not obj.video_id:
            import urlparse
            url_data = urlparse.urlparse(obj.video)
            query = urlparse.parse_qs(url_data.query)
            obj.video_id = query["v"][0]
        obj.foto = "http://img.youtube.com/vi/%s/1.jpg" % (obj.video_id)
        import urllib
        from lxml import etree
        import StringIO
        xmlFile = "http://gdata.youtube.com/feeds/api/videos/%s" % obj.video_id
        xmlData = urllib.urlopen(xmlFile).read()
        fakeFile= StringIO.StringIO( xmlData)
        context = etree.iterparse(fakeFile)
        elemento_dict = {}
        elementos = []
        for action, elem in context:
            if not elem.text:
                text = "None"
            else:
                text = elem.text
            elem.tag = elem.tag.replace('{http://search.yahoo.com/mrss/}','')
            elemento_dict[elem.tag] = text
            if elem.tag == "description":
                elementos.append(elemento_dict)
                elemento_dict = {}
        obj.save()

admin.site.register(Video, VideoAdmin)

class BannerAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Banner", {'fields': ['posicao', 'titulo', 'url', 'status']}), ("Upload", {'fields': ['arquivo']})
    ]
    list_display = ('titulo', 'posicao')
    list_filter = ['posicao', 'titulo']
    search_fields = ['titulo', 'posicao']
    date_hierarchy = 'publicado_em'

admin.site.register(Banner, BannerAdmin)