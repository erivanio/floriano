# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.template import RequestContext
from django.shortcuts import render_to_response
from apps.multimidia.models import Album, Video
from django.shortcuts import get_object_or_404
from datetime import datetime


def album(request, slug, id):
    album = get_object_or_404(Album, slug = slug,id=id, status = True)
    albuns = Album.objects.filter(status=True).exclude(pk= album.pk).exclude(publicado_em__gte=datetime.now()).order_by('-publicado_em')[:8]
    try:
        titulo = album.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('floriano/multimidia/fotos.html', locals(),
                              context_instance=RequestContext(request))

def central_albuns(request):
    albuns = Album.objects.filter(status=True).exclude(publicado_em__gte=datetime.now()).order_by('-publicado_em')
    try:
        titulo = 'Álbuns'
    except:
        titulo = ''

    return render_to_response('floriano/multimidia/albuns.html', locals(),
                              context_instance=RequestContext(request))

def videos(request):
    videos = Video.objects.all()
    paginator = Paginator(videos, 4)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        videos = paginator.page(page)
    except (EmptyPage, InvalidPage):
        videos = paginator.page(paginator.num_pages)
    # agenda_passados = Agenda.objects.filter(horario__lte=datetime.now)

    return render_to_response('floriano/multimidia/videos.html', locals(),
        context_instance=RequestContext(request))