Django Skeleton Application
======================

This is a simple skeleton for a django project with some settings already setup.
Clone it, rename the django_skeleton folder with your project name, set PROJECT_NAME on your settings with the same name of the folder, set your SECRET_KEY and be happy :)

This project is intended to run on Django version 1.4+.